#include "HODLR.hpp"

#define ASSERT(left,operator,right) { if(!((left) operator (right))){ std::cerr << "ASSERT FAILED: " << #left << #operator << #right << " @ " << __FILE__ << " (" << __LINE__ << "). " << #left << "=" << (left) << "; " << #right << "=" << (right) << std::endl; exit(-1);} }


void HODLR::concatenate(double* &U, double* &V, double* &D) {

    assert(tolerance >= 1);
    int rank = tolerance;

    Mat bigU(N, n_levels*rank);
    Mat bigV(N, n_levels*rank);

    // Initializing for the non-leaf levels:
    #pragma omp parallel for
    for(int j = 0; j < n_levels; j++) {
        for(int k = 0; k < nodes_in_level[j]; k++) {
            ASSERT(rank, ==, tree[j][k]->rank[0]);
            ASSERT(rank, ==, tree[j][k]->rank[1]);

            int row0 = tree[j][k]->c_start[0];
            int row1 = tree[j][k]->c_start[1];
            int size0 = tree[j][k]->c_size[0];
            int size1 = tree[j][k]->c_size[1];
            bigU.block(row0, j*rank, size0, rank) = tree[j][k]->U[0];
            bigU.block(row1, j*rank, size1, rank) = tree[j][k]->U[1];

            bigV.block(row0, j*rank, size0, rank) = tree[j][k]->V[0];
            bigV.block(row1, j*rank, size1, rank) = tree[j][k]->V[1];
        }
    }

    int n_leaf = tree[n_levels][0]->n_size;
    int batchSize = nodes_in_level[n_levels];
   
    Mat bigD(n_leaf, n_leaf*batchSize);

    #pragma omp parallel for
    for (int k=0; k < batchSize; k++) {
        assert(n_leaf == tree[n_levels][k]->n_size);
        bigD.middleCols(k*n_leaf, n_leaf) = tree[n_levels][k]->K;
    }

    std::memcpy(U, bigU.data(), N*n_levels*rank*sizeof(double));
    std::memcpy(V, bigV.data(), N*n_levels*rank*sizeof(double));
    std::memcpy(D, bigD.data(), n_leaf*n_leaf*batchSize*sizeof(double));
}

