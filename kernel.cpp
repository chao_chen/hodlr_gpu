// This file is a modification of tutorial.cpp at https://github.com/sivaramambikasaran/HODLR/blob/master/examples/tutorial.cpp.

#include "HODLR_Matrix.hpp"
#include "HODLR.hpp"
#include "KDTree.hpp"

#include "gpu_solver.hpp"

// Derived class of HODLR_Matrix which is ultimately
// passed to the HODLR_Tree class:
class Kernel : public HODLR_Matrix 
{

private:
    Mat x;

public:

    // Constructor:
    Kernel(int N, int dim) : HODLR_Matrix(N) 
    {
        x = (Mat::Random(N, dim)).real();
        // This is being sorted to ensure that we get
        // optimal low rank structure:
        getKDTreeSorted(x, 0);
    };
    
    // In this example, we are illustrating usage using
    // the gaussian kernel:
    dtype getMatrixEntry(int i, int j) 
    {
        size_t dim = x.cols();

        // Value on the diagonal:
        if(i == j)
        {
            return 10;
        }
        
        // Otherwise:
        else
        {   
            dtype R2;
            // Initializing:
            R2 = 0;

            for(unsigned k = 0; k < dim; k++) 
            {
                R2 += (x(i,k) - x(j,k)) * (x(i,k) - x(j,k));
            }

            // Gaussian Kernel: e(-R^2)
            return exp(-R2);
        }
    }

    // Destructor:
    ~Kernel() {};
};

int main(int argc, char* argv[]) 
{
    // default arguments
    int M = 200, dim = 1, L = 5;
    double tolerance = pow(10, -12);
    bool showTree = false;
    std::string lowrank = "SVD";
    int repeat = 1; // repeat for accurate timing 
    int nrhs = 1;

    for (int i=0; i<argc; i++) {
        if (!strcmp(argv[i], "-m"))
            M = atoi(argv[i+1]);
        if (!strcmp(argv[i], "-d"))
            dim = atoi(argv[i+1]);
        if (!strcmp(argv[i], "-t"))
            tolerance = atof(argv[i+1]);
        if (!strcmp(argv[i], "-s"))
            if (atoi(argv[i+1]) > 0)
                showTree = true;
        if (!strcmp(argv[i], "-lowrank"))
            lowrank = std::string(argv[i+1]);
        if (!strcmp(argv[i], "-repeat"))
            repeat = atoi(argv[i+1]);
        if (!strcmp(argv[i], "-nrhs"))
            nrhs = atoi(argv[i+1]);
        if (!strcmp(argv[i], "-L")) {
            L = atoi(argv[i+1]);
        }
    }
    int N = (1<<L)*M;

    // Declaration of HODLR_Matrix object that abstracts data in Matrix:
    Kernel* K = new Kernel(N, dim);
    std::cout << "========================= Problem Parameters =========================" << std::endl;
    std::cout << "Matrix Size                        :" << N << std::endl;
    std::cout << "Leaf Size                          :" << M << std::endl;
    std::cout << "Tree Level                         :" << L << std::endl;
    std::cout << "Dimensionality                     :" << dim << std::endl;
    std::cout << "Tolerance                          :" << tolerance << std::endl;
    std::cout << "low rank method                    :" << lowrank << std::endl;
    std::cout << "repeat for timing                  :" << repeat << std::endl;
    std::cout << "# of rhs                           :" << nrhs << std::endl;
    std::cout << std::endl;

    // Variables used in timing:
    double start, end;

    // Storing Time Taken:
    double hodlr_time; //exact_time;
    std::cout << "========================= Assembly Time =========================" << std::endl;
    // If we are assembling a symmetric matrix:
    bool is_sym = false;
    // If we know that the matrix is also PD:
    // By setting the matrix to be symmetric-positive definite, 
    // we trigger the fast symmetric factorization method to be used
    // In all other cases the fast factorization method is used
    bool is_pd = false;
    start = omp_get_wtime();
    // Creating a pointer to the HODLR Tree structure:
    HODLR* T = new HODLR(N, M, tolerance);
    T->assemble(K, lowrank, is_sym, is_pd);
    end = omp_get_wtime();
    if (repeat > 1) {
        start = omp_get_wtime();
        for (int i=0; i<repeat; i++)
            T->assemble(K, lowrank, is_sym, is_pd);
        end   = omp_get_wtime();    
    }
    hodlr_time = (end - start)/repeat;
    std::cout << "Time for assembly in HODLR form    :" << hodlr_time << std::endl;

   

    std::cout << "\n========================= Factorization =========================" << std::endl;
    start = omp_get_wtime();
    T->factorize();
    end   = omp_get_wtime();
    if (repeat > 1) {
        start = omp_get_wtime();
        for (int i=0; i<repeat; i++)
            T->factorize();
        end   = omp_get_wtime();    
    }
    hodlr_time = (end - start)/repeat;
    std::cout << "Time of cpu factorize              :" << hodlr_time << std::endl;
   


    std::cout << "========================= Solving =========================" << std::endl;
    
    Mat b_exact = (Mat::Random(N, nrhs)).real();
    Mat x_fast;
    start  = omp_get_wtime();
    x_fast = T->solve(b_exact);
    end    = omp_get_wtime();
    if (repeat > 1) {
        start = omp_get_wtime();
        for (int i=0; i<repeat; i++)
            x_fast = T->solve(b_exact);
        end   = omp_get_wtime();    
    }
    hodlr_time = (end - start)/repeat;
    std::cout << "Time of cpu solve                  :" << hodlr_time << std::endl;
    
    
    std::cout << "========================= GPU =========================" << std::endl;

    // Create new data structure
    if (showTree) T->printTreeDetails();
    
    int rank = tolerance;
    assert(N%M == 0);
    assert(tolerance > 1);

        
    start = omp_get_wtime();
    double *hU = new double [N*rank*L];
    double *hV = new double [N*rank*L];
    double *hD = new double [N*M];
        
    T->concatenate(hU, hV, hD);
    end = omp_get_wtime();
    std::cout<<"Time for assembly big U, V, and D matrices: "<<end-start<<std::endl;
    
    std::vector<int> ranks(L, rank);
    gpu_solver(N, M, L, ranks, hU, hV, hD, b_exact.data(), x_fast.data(), b_exact.cols());

    delete []hU;
    delete []hV;
    delete []hD;

    
    delete T;
    //delete K;


    return 0;
}
