#ifndef gpu_solver_hpp
#define gpu_solver_hpp

void gpu_solver(int N, int n, int L, const std::vector<int> &ranks,
        const double *hU, const double *hV, const double *hD, 
        const double *b, const double *x, int nrhs);

#endif
