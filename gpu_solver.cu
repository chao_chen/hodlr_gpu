#include "hodlr_gpu.hpp"
#include "timer_gpu.hpp"

void gpu_solver(int N, int n, int L, const std::vector<int> &ranks,
        const double *hU, const double *hV, const double *hD, 
        const double *b, const double *x, int nrhs) {

    const int repeat = 3;

    TimerGPU t;
    t.start();
    HODLR_GPU<double> hodlr_gpu(N, n, L, ranks);
    hodlr_gpu.initialize(hU, hV, hD);
    t.stop();
    std::cout<<"Time for initializing HODLR GPU: "<<t.elapsed_time()<<std::endl;

    double gpu_time = 0.;
    t.start();
    hodlr_gpu.factorize();
    t.stop();    
    gpu_time = t.elapsed_time();

    if (repeat > 1) {
        gpu_time = 0.;
        for (int i=0; i<repeat; i++) {
            hodlr_gpu.initialize(hU, hV, hD);
        
            t.start(); 
            hodlr_gpu.factorize();
            t.stop();
            gpu_time += t.elapsed_time();
        }
        gpu_time /= repeat;
    }
    std::cout << "Time of gpu factorization          :" << gpu_time << std::endl;


    double *xh = new double [N*nrhs];
    std::copy(b, b+N*nrhs, xh);

    t.start();  
    hodlr_gpu.solve(xh, nrhs);
    t.stop();    
    gpu_time = t.elapsed_time();

    if (repeat > 1) {
        gpu_time = 0.;
        for (int i=0; i<repeat; i++) {
            std::copy(b, b+N*nrhs, xh);
        
            t.start(); 
            hodlr_gpu.solve(xh, nrhs);
            t.stop();   
            gpu_time += t.elapsed_time();
        }
        gpu_time /= repeat;
    }
    std::cout << "Time of gpu solve                  :" << gpu_time << std::endl;
    
    
    double err = 0., nrm = 0.;
    for (int i=0; i<N*nrhs; i++) {
        nrm = x[i]*x[i];
        err = (x[i]-xh[i])*(x[i]-xh[i]);
    }
    std::cout << "Error of gpu solve                 :" << sqrt(err/nrm) << std::endl;
}

