#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "hodlr_gpu.hpp"

template <typename T>
void read_matrix(std::string bname, int N, int &nrhs, T *&b);

int main(int argc, char* argv[]) {
    
    std::string fname;

    for (int i=1; i<argc; i++) {
        if (!strcmp(argv[i], "-f"))
            fname = std::string(argv[i+1]);
    }
    auto pos = fname.find("hodlr");
    std::string bname = fname, xname = fname;
    bname.replace(pos, 5, "rhs");
    xname.replace(pos, 5, "x");

    std::cout<<"Read file: "<<fname<<std::endl;

    std::fstream file(fname.c_str(), std::ios::in|std::ios::binary);
    assert(file.is_open() && "Can't open file");

    int N=0, m=0, L=0;
    file.read((char*)&N, sizeof(int));
    file.read((char*)&m, sizeof(int));
    file.read((char*)&L, sizeof(int));
    //file >> N >> m >> L;
    std::cout<<"[HODLR matrix] "
        <<"N: "<<N
        <<", m: "<<m
        <<", L: "<<L
        <<std::endl;
    assert(N>0 && m<1e4 && L<1e3);

    std::vector<int> ranks(L);
    std::vector<int> colptr(L+1, 0);
    
    std::cout<<"ranks from level 1 to L: ";
    for (int i=0; i<L; i++) {
        file.read((char*)&ranks[i], sizeof(int));
        colptr[i+1] = colptr[i] + ranks[i];
        std::cout<<ranks[i]<<" ";
    }
    std::cout<<std::endl;
    
    int nCol = colptr[L];
    std::cout<<"Total number of columns in U and V: "<<nCol<<std::endl;

    dtype *U, *V, *D;

    TimerGPU t;
    t.start();
    CHECK_CUDA( cudaMallocHost(&U, N*nCol*sizeof(dtype)) );
    CHECK_CUDA( cudaMallocHost(&V, N*nCol*sizeof(dtype)) );
    CHECK_CUDA( cudaMallocHost(&D, N*m*sizeof(dtype)) );
    t.stop();
    std::cout<<"Allocate "<<1.e-9*sizeof(dtype)*N*(2*nCol+m)<<" GB took "
        <<t.elapsed_time()<<" s"<<std::endl;

    t.start();
    file.read((char*)U, N*nCol*sizeof(dtype));
    file.read((char*)V, N*nCol*sizeof(dtype));
    file.read((char*)D, N*m*sizeof(dtype));
    t.stop();
    std::cout<<"Read file took: "<<t.elapsed_time()<<" s"<<std::endl;

    assert(N>3 && nCol>3 && m>3 && "Output small submatrices");
    std::cout<<"[Sanity check] U(1:3,1:3)="<<std::endl;
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++)
            std::cout<<U[i+j*N]<<" ";
        std::cout<<std::endl;
    }
    std::cout<<std::endl;

    std::cout<<"[Sanity check] V(1:3,1:3)="<<std::endl;
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++)
            std::cout<<V[i+j*N]<<" ";
        std::cout<<std::endl;
    }
    std::cout<<std::endl;

    std::cout<<"[Sanity check] D(1:3,1:3)="<<std::endl;
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++)
            std::cout<<D[i+j*m]<<" ";
        std::cout<<std::endl;
    }
    std::cout<<std::endl;
    
    file.close();

    int nb, nx;
    dtype *b, *x;

    read_matrix<dtype>(bname, N, nb, b);
    read_matrix<dtype>(xname, N, nx, x);

    assert(nb == nx);
    int nrhs = nb;

    /*********/
    /* HODLR */
    /*********/

    t.start();
    HODLR_GPU<dtype> hmat(N, m, L, ranks);
    hmat.initialize(U, V, D);
    t.stop();
    std::cout<<"Time for initializing HODLR GPU: "<<t.elapsed_time()<<std::endl;

    double gpu_time = 0.;
    t.start();
    hmat.factorize();
    t.stop();    
    gpu_time = t.elapsed_time();
    std::cout << "Time of gpu factorization: " << gpu_time << std::endl;

    std::vector<dtype> xh(b, b+N*nrhs);
        
    std::cout<<"[Sanity check] x(1:3,:)="<<std::endl;
    for (int i=0; i<3; i++) {
        for (int j=0; j<nrhs; j++)
            std::cout<<xh[i+j*N]<<" ";
        std::cout<<std::endl;
    }
    std::cout<<std::endl;

    t.start();
    hmat.solve(xh.data(), nrhs);
    t.stop();
    gpu_time = t.elapsed_time();

    double err = 0., nrm = 0.;
    for (int i=0; i<N*nrhs; i++) {
        nrm += abs(x[i])*abs(x[i]);
        err += abs(x[i]-xh[i])*abs(x[i]-xh[i]);
    }

    std::cout << "Time of gpu solve: " << gpu_time << std::endl;
    std::cout << "Error of gpu solve: " << sqrt(err/nrm) << std::endl;

    std::cout<<"[Sanity check] xh(1:3,:)="<<std::endl;
    for (int i=0; i<3; i++) {
        for (int j=0; j<nrhs; j++)
            std::cout<<xh[i+j*N]<<" ";
        std::cout<<std::endl;
    }
    std::cout<<std::endl;

    return 0;
}

template <typename T>
void read_matrix(std::string bname, int N, int &nrhs, T *&b) {

    std::cout<<"Read file: "<<bname<<std::endl;
    std::ifstream file; 
    file.open(bname.c_str(), std::ios::in|std::ios::binary);
    assert(file.is_open() && "Can't open file");
    
    int M=0;
    file.read((char*)&M, sizeof(int));
    file.read((char*)&nrhs, sizeof(int));
    std::cout<<"[RHS] "
        <<"N: "<<M
        <<", nrhs: "<<nrhs
        <<std::endl;
    assert(M==N && nrhs<1e3);

    //double *b;

    TimerGPU t;
    t.start();
    CHECK_CUDA( cudaMallocHost(&b, M*nrhs*sizeof(T)) );
    file.read((char*)b, M*nrhs*sizeof(T));
    t.stop();
    std::cout<<"Read file took: "<<t.elapsed_time()<<" s"<<std::endl;

    std::cout<<"[Sanity check] matrix(1:3,:)="<<std::endl;
    for (int i=0; i<3; i++) {
        for (int j=0; j<nrhs; j++)
            std::cout<<b[i+j*N]<<" ";
        std::cout<<std::endl;
    }
    std::cout<<std::endl;
    
    file.close();
}
    

