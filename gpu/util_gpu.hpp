#ifndef UTIL_GPU_HPP
#define UTIL_GPU_HPP

#include <cuda_runtime.h>
#include "cublas_v2.h"

#include <stdio.h>         // printf
#include <stdlib.h>        // EXIT_FAILURE
#include <assert.h>
#include <cstring>


#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>

template <typename T>
using TVEC = thrust::device_vector<T>;

typedef TVEC<int> ivec;
typedef TVEC<float> fvec;
typedef TVEC<double> dvec;


/*
template <typename T>
using ptr = thrust::device_ptr<T>;

typedef ptr<int> iptr;
typedef ptr<float> fptr;
typedef ptr<double> dptr;
*/

std::ostream& operator<<(std::ostream& os, const cuDoubleComplex &z)
{
    os<<z.x;
    if (z.y >= 0) os<<" + "<<z.y<<"i";
    else os<<" - "<<-z.y<<"i";
    return os;
}

std::ostream& operator<<(std::ostream& os, const cuComplex &z)
{
    os<<z.x;
    if (z.y >= 0) os<<" + "<<z.y<<"i";
    else os<<" - "<<-z.y<<"i";
    return os;
}

cuComplex operator -(const cuComplex z1, const cuComplex z2) {
    return {z1.x-z2.x, z1.y-z2.y};
}

cuDoubleComplex operator -(const cuDoubleComplex z1, const cuDoubleComplex z2) {
    return {z1.x-z2.x, z1.y-z2.y};
}

cuComplex operator *(const cuComplex z1, const cuComplex z2) {
    return {z1.x*z2.x-z1.y*z2.y, z1.x*z2.y+z1.y*z2.x};
}

cuDoubleComplex operator *(const cuDoubleComplex z1, const cuDoubleComplex z2) {
    return {z1.x*z2.x-z1.y*z2.y, z1.x*z2.y+z1.y*z2.x};
}

double norm2(const cuComplex z) {
    return z.x*z.x + z.y*z.y;
}

double norm2(const cuDoubleComplex z) {
    return z.x*z.x + z.y*z.y;
}

template <typename T>
void tprint(const thrust::device_vector<T>& vec, const std::string &name) {
  std::cout<<name<<":"<<std::endl;
  for (int i=0; i<vec.size(); i++)
    std::cout<<vec[i]<<" ";
  std::cout<<std::endl<<std::endl;
}

template <typename T>
void tprint(int m, int n, const thrust::device_vector<T>& vec, const std::string &name) {
  std::cout<<std::endl<<name<<":"<<std::endl;
  for (int i=0; i<m; i++) {
    for (int j=0; j<n; j++)
      std::cout<<vec[i+j*m]<<" ";
    std::cout<<std::endl;
  }
  std::cout<<std::endl;
}


#define CHECK_CUDA(func)                                                       \
{                                                                              \
    cudaError_t status = (func);                                               \
    if (status != cudaSuccess) {                                               \
        printf("CUDA API failed at line %d with error: %s (%d)\n",             \
               __LINE__, cudaGetErrorString(status), status);                  \
        assert(false);                                                         \
    }                                                                          \
}


static const char *cudaGetErrorEnum(cublasStatus_t error) {
    switch (error) {
        case CUBLAS_STATUS_SUCCESS:
            return "CUBLAS_STATUS_SUCCESS";

        case CUBLAS_STATUS_NOT_INITIALIZED:
            return "CUBLAS_STATUS_NOT_INITIALIZED";

        case CUBLAS_STATUS_ALLOC_FAILED:
            return "CUBLAS_STATUS_ALLOC_FAILED";

        case CUBLAS_STATUS_INVALID_VALUE:
            return "CUBLAS_STATUS_INVALID_VALUE";

        case CUBLAS_STATUS_ARCH_MISMATCH:
            return "CUBLAS_STATUS_ARCH_MISMATCH";

        case CUBLAS_STATUS_MAPPING_ERROR:
            return "CUBLAS_STATUS_MAPPING_ERROR";

        case CUBLAS_STATUS_EXECUTION_FAILED:
            return "CUBLAS_STATUS_EXECUTION_FAILED";

        case CUBLAS_STATUS_INTERNAL_ERROR:
            return "CUBLAS_STATUS_INTERNAL_ERROR";
    }
    return "<unknown>";
}

#define CHECK_CUBLAS(ans) { cublasAssert((ans), __FILE__, __LINE__); }
inline void cublasAssert(cublasStatus_t code, const char *file, int line, bool abort=true) {
   if (code != CUBLAS_STATUS_SUCCESS) {
      fprintf(stderr,"CUBLAS assert: %s %s %d\n", cudaGetErrorEnum(code), file, line);
      if (abort) exit(code);
   }
}


#endif
