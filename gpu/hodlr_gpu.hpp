#ifndef HODLR_GPU_TASKS_HPP
#define HODLR_GPU_TASKS_HPP

#include "util_gpu.hpp"
#include "timer_gpu.hpp"
#include <numeric>

    
typedef cuComplex Complex;
typedef cuDoubleComplex Zomplex;


#ifdef USE_FLOAT
    using dtype = float;
#elif USE_DOUBLE
    using dtype = double;
#elif USE_DOUBLE_COMPLEX
    using dtype = Zomplex;
#elif USE_SINGLE_COMPLEX
    using dtype = Complex;
#endif


template <typename T>
class HODLR_GPU {
    public:
        //HODLR_GPU(int n_, int m_, int r_, int L_);
        HODLR_GPU(int N_, int n_, int L_, const std::vector<int> &ranks);

        void initialize(const T *hU, const T *hV, const T *hD);
        void factorize();
        void solve(T *b, int nrhs);
        
        ~HODLR_GPU();

    private:
        void XgetrfBatched(int n, double *const Aarray[], int lda, int *PivotArray,
                int *infoArray, int batchSize);
        void XgetrfBatched(int n, float *const Aarray[], int lda, int *PivotArray,
                int *infoArray, int batchSize);
        void XgetrfBatched(int n, Complex *const Aarray[], int lda, int *PivotArray,
                int *infoArray, int batchSize);
        void XgetrfBatched(int n, Zomplex *const Aarray[], int lda, int *PivotArray,
                int *infoArray, int batchSize);

        void XgetrsBatched(cublasOperation_t trans, int n, int nrhs, 
                const double *const Aarray[], int lda, const int *devIpiv, 
                double *const Barray[], int ldb, int *info, int batchSize);
        void XgetrsBatched(cublasOperation_t trans, int n, int nrhs, 
                const float *const Aarray[], int lda, const int *devIpiv, 
                float *const Barray[], int ldb, int *info, int batchSize);
        void XgetrsBatched(cublasOperation_t trans, int n, int nrhs, 
                const Complex *const Aarray[], int lda, const int *devIpiv, 
                Complex *const Barray[], int ldb, int *info, int batchSize);
        void XgetrsBatched(cublasOperation_t trans, int n, int nrhs, 
                const Zomplex *const Aarray[], int lda, const int *devIpiv, 
                Zomplex *const Barray[], int ldb, int *info, int batchSize);

        void XgemmBatched(cublasOperation_t, cublasOperation_t, int, int, int,
                double*, double*, int, int, double*, int, int, double*,
                double*, int, int, int, int);
        void XgemmBatched(cublasOperation_t, cublasOperation_t, int, int, int,
                float*, float*, int, int, float*, int, int, float*,
                float*, int, int, int, int);
        void XgemmBatched(cublasOperation_t, cublasOperation_t, int, int, int,
                Complex*, Complex*, int, int, Complex*, int, int, Complex*,
                Complex*, int, int, int, int);
        void XgemmBatched(cublasOperation_t, cublasOperation_t, int, int, int,
                Zomplex*, Zomplex*, int, int, Zomplex*, int, int, Zomplex*,
                Zomplex*, int, int, int, int);
            
    private:

#if defined(USE_FLOAT) || defined(USE_DOUBLE)
        dtype ZERO  = 0.;
        dtype ONE   = 1.;
        dtype NEGONE = -1.;
        cublasOperation_t TRANST = CUBLAS_OP_T;
        cublasOperation_t TRANSN = CUBLAS_OP_N;
        int CFLOP = 2;
#elif defined(USE_DOUBLE_COMPLEX) || defined(USE_SINGLE_COMPLEX)
        dtype ZERO  = {0., 0.};
        dtype ONE   = {1., 0.};
        dtype NEGONE = {-1., 0.};
        cublasOperation_t TRANST = CUBLAS_OP_C;
        cublasOperation_t TRANSN = CUBLAS_OP_N;
        int CFLOP = 8;
#endif

        int n; // leaf size
        int m; // number of leaves
        int L; // tree levels

        int N; // problem/matrix size
        int C; // number of columns in the big U matrix

        std::vector<int> ranks;
        std::vector<int> colptr;

        //-------------------------
        // device memory
        //-------------------------
        
        // low-rank bases
        TVEC<T> U, V;
        T *ptrU, *ptrV;

        // diagonal blocks
        TVEC<T>  D; 
        TVEC<T*> Darray;
        T **ptrD;
        
        ivec pivotsD; // pivots of diagonal blocks
        int *ptrPivotsD;

        // matrices at non-leaf nodes
        TVEC<T>  K;
        TVEC<T*> Karray;
        std::vector<int> lvp; // pointers to the start of every level
        ivec pivotsK; // pivots for matrices at non-leaf nodes
        std::vector<int*> ptrPivotsK;

        // cublas related
        cublasHandle_t handle;

        int fBatch, sBatch;
        std::vector<cudaStream_t> streams;
};


template <typename T>
struct Stride: public thrust::unary_function<int, int> {
    T  *first;
    int stride;

    __host__ __device__
        Stride(T *f, int s) {
            first = f;
            stride = s;
        }

    __host__ __device__
        T* operator()(int i) {
            return first + i*stride;
        }
};


// indices of two diagonal blocks in K
struct KDB: public thrust::unary_function<int, int> {
    int r, blk, kblk, kcol;

    __host__ __device__
        KDB(int r_) {
            r    = r_;
            blk  = r*r;   // block size in vtu
            kblk = 2*r*r; // panel size in K (a vtu plus an identity)
            kcol = 2*r;   // column size in K
        }

    __host__ __device__
        int operator()(int i) {
            int j = i/blk; // which block
            int k = i%blk; // indices within a block
            int row = k%r;   // row in the block
            int col = k/r;   // column in the block
            int ofs = j%2*r; // offset in a panel of K
            return j*kblk + ofs + row + col*kcol;
        }
};


struct KOB: public thrust::unary_function<int, int> {
    int r, kcol;

    __host__ __device__
        KOB(int r_) {
            r     = r_;
            kcol = 2*r;
        }

    __host__ __device__
        int operator()(int i) {
            return i*kcol + (i+r) % kcol;
        }
};


/*
    Every leaf box has 'n' points/indices, and there are 'm' leaves.
    The big matrix of bases has size ('n' x 'm') times ('r' x 'L'), where 'r'
      is the (constant/fixed) rank and 'L' is the number of tree levels.
 */
template <typename T>
HODLR_GPU<T>::HODLR_GPU(int N_, int n_, int L_, const std::vector<int> &ranks_): 
    N(N_), n(n_), L(L_), ranks(ranks_) {
    
    m = 1<<L;

    colptr.resize(L+1, 0);
    for (int i=0; i<L; i++)
        colptr[i+1] = colptr[i] + ranks[i];

    C = colptr[L]; // number of columns in U
    
    assert(N == n*m); // number of rows in U
    assert(ranks.size() == L);
    
    std::cout<<"\nCreated HODLR_GPU object:\n"
        <<"  tree level: "<<L
        <<"\n  leaf size: "<<n
        <<"\n  # of leaves: "<<m
        <<"\n  N: "<<N
        <<"\n  C: "<<C
        <<"\n\n";

    TimerGPU t; 
    t.start();
    CHECK_CUBLAS( cublasCreate_v2(&handle) );
    // tuned for V100 on mimir
    fBatch = 1<<6;
    sBatch = 1<<3;
    streams.resize(fBatch);
    for (int i=0; i<fBatch; i++)
        cudaStreamCreate(&streams[i]);
    t.stop();
    std::cout<<"Created CUBLAS handle and CUDA streams: "<<t.elapsed_time()
        <<" s"<<"\n\n";
}


template <typename T>
void HODLR_GPU<T>::initialize(const T *hU, const T *hV, const T *hD) {

    TimerGPU t;
    double tc = 0.;

    // low-rank bases
    U.resize(N*C);
    V.resize(N*C);
    ptrU = thrust::raw_pointer_cast(U.data());
    ptrV = thrust::raw_pointer_cast(V.data());

    t.start();
    thrust::copy_n(hU, N*C, U.begin());
    thrust::copy_n(hV, N*C, V.begin());
    t.stop();
    tc += t.elapsed_time();

    // diagonal blocks
    D.resize(N*n);

    Darray.resize(m);
    T *ptr = thrust::raw_pointer_cast(D.data());
    auto zero = thrust::make_counting_iterator<int>(0);
    thrust::transform(zero, zero+m, Darray.begin(), Stride<T>(ptr, n*n));
    ptrD = thrust::raw_pointer_cast(Darray.data());

    pivotsD.resize(N);
    ptrPivotsD = thrust::raw_pointer_cast(pivotsD.data());

    t.start();
    thrust::copy_n(hD, N*n, D.begin());
    t.stop();
    tc += t.elapsed_time();

    // matrices at non-leaf nodes
    lvp.resize(L+1, 0);
    for (int i=0; i<L; i++) {
        int numK = 1<<i;
        lvp[i+1] = lvp[i] + numK*4*ranks[i]*ranks[i];
    }

    K.resize(lvp[L]);
    thrust::fill(K.begin(), K.end(), ZERO); // initialize to 0

    int nk = (1<<L) - 1;
    Karray.resize(nk);
    for (int i=0; i<L; i++) {
        int preK = (1<<i) - 1;
        int numK = 1<<i;
        ptr = thrust::raw_pointer_cast(K.data()+lvp[i]);
        thrust::transform(zero, zero+numK, Karray.begin()+preK,
                Stride<T>(ptr, 4*ranks[i]*ranks[i]));
    }

    // pivots of K matrices
    int np = 0;
    for (int i=0; i<L; i++) {
        int numK = 1<<i;
        np += numK*2*ranks[i];
    }
    pivotsK.resize(np);

    ptrPivotsK.resize(L+1);
    ptrPivotsK[0] = thrust::raw_pointer_cast(pivotsK.data());
    for (int i=0; i<L; i++) {
        int numK = 1<<i;
        ptrPivotsK[i+1] = ptrPivotsK[i] + numK*2*ranks[i];
    }

    std::cout<<"\nCopy data from CPU to GPU: "<<tc<<" s, bandwidth: "
        <<1e-9*(2*C+n)*N*sizeof(T)/tc<<" GB/s\n\n";
}


template <typename T>
void HODLR_GPU<T>::XgemmBatched(cublasOperation_t OP1, cublasOperation_t OP2,
        int m_, int n_, int k_, double *alpha, double *A, int lda, int strideA,
        double *B, int ldb, int strideB, double *beta, double *C, int ldc,
        int strideC, int count, int nBatch) {

    if (count > nBatch) {
        CHECK_CUBLAS( cublasDgemmStridedBatched(handle, OP1, OP2,
                    m_, n_, k_, alpha, A, lda, strideA, B, ldb, strideB, beta, C,
                    ldc, strideC, count) );
    } else {
        for (int i=0; i<count; i++) {
            CHECK_CUBLAS( cublasSetStream(handle, streams[i]) );
            CHECK_CUBLAS( cublasDgemm(handle, OP1, OP2, m_, n_, k_, alpha,
                        A+i*strideA, lda, B+i*strideB, ldb, beta, C+i*strideC, ldc) );
        }
    }
}


template <typename T>
void HODLR_GPU<T>::XgemmBatched(cublasOperation_t OP1, cublasOperation_t OP2,
        int m_, int n_, int k_, float *alpha, float *A, int lda, int strideA,
        float *B, int ldb, int strideB, float *beta, float *C, int ldc,
        int strideC, int count, int nBatch) {

    if (count > nBatch) {
        CHECK_CUBLAS( cublasSgemmStridedBatched(handle, OP1, OP2,
                    m_, n_, k_, alpha, A, lda, strideA, B, ldb, strideB, beta, C,
                    ldc, strideC, count) );
    } else {
        for (int i=0; i<count; i++) {
            CHECK_CUBLAS( cublasSetStream(handle, streams[i]) );
            CHECK_CUBLAS( cublasSgemm(handle, OP1, OP2, m_, n_, k_, alpha,
                        A+i*strideA, lda, B+i*strideB, ldb, beta, C+i*strideC, ldc) );
        }
    }
}


template <typename T>
void HODLR_GPU<T>::XgemmBatched(cublasOperation_t OP1, cublasOperation_t OP2,
        int m_, int n_, int k_, Complex *alpha, Complex *A, int lda, int strideA,
        Complex *B, int ldb, int strideB, Complex *beta, Complex *C, int ldc,
        int strideC, int count, int nBatch) {

    if (count > nBatch) {
        CHECK_CUBLAS( cublasCgemmStridedBatched(handle, OP1, OP2,
                    m_, n_, k_, alpha, A, lda, strideA, B, ldb, strideB, beta, C,
                    ldc, strideC, count) );
    } else {
        for (int i=0; i<count; i++) {
            CHECK_CUBLAS( cublasSetStream(handle, streams[i]) );
            CHECK_CUBLAS( cublasCgemm(handle, OP1, OP2, m_, n_, k_, alpha,
                        A+i*strideA, lda, B+i*strideB, ldb, beta, C+i*strideC, ldc) );
        }
    }
}


template <typename T>
void HODLR_GPU<T>::XgemmBatched(cublasOperation_t OP1, cublasOperation_t OP2,
        int m_, int n_, int k_, Zomplex *alpha, Zomplex *A, int lda, int strideA,
        Zomplex *B, int ldb, int strideB, Zomplex *beta, Zomplex *C, int ldc,
        int strideC, int count, int nBatch) {

    if (count > nBatch) {
        CHECK_CUBLAS( cublasZgemmStridedBatched(handle, OP1, OP2,
                    m_, n_, k_, alpha, A, lda, strideA, B, ldb, strideB, beta, C,
                    ldc, strideC, count) );
    } else {
        for (int i=0; i<count; i++) {
            CHECK_CUBLAS( cublasSetStream(handle, streams[i]) );
            CHECK_CUBLAS( cublasZgemm(handle, OP1, OP2, m_, n_, k_, alpha,
                        A+i*strideA, lda, B+i*strideB, ldb, beta, C+i*strideC, ldc) );
        }
    }
}


template <typename T>
void HODLR_GPU<T>::XgetrfBatched(int n, double *const Aarray[], int lda, int *PivotArray,
        int *infoArray, int batchSize) {
    CHECK_CUBLAS( cublasDgetrfBatched(handle, n, Aarray, lda, PivotArray,
                infoArray, batchSize) );
}


template <typename T>
void HODLR_GPU<T>::XgetrfBatched(int n, float *const Aarray[], int lda, int *PivotArray,
        int *infoArray, int batchSize) {
    CHECK_CUBLAS( cublasSgetrfBatched(handle, n, Aarray, lda, PivotArray,
                infoArray, batchSize) );
}


template <typename T>
void HODLR_GPU<T>::XgetrfBatched(int n, Complex *const Aarray[], int lda, int *PivotArray,
        int *infoArray, int batchSize) {
    CHECK_CUBLAS( cublasCgetrfBatched(handle, n, Aarray, lda, PivotArray,
                infoArray, batchSize) );
}


template <typename T>
void HODLR_GPU<T>::XgetrfBatched(int n, Zomplex *const Aarray[], int lda, int *PivotArray,
        int *infoArray, int batchSize) {
    CHECK_CUBLAS( cublasZgetrfBatched(handle, n, Aarray, lda, PivotArray,
                infoArray, batchSize) );
}


template <typename T>
void HODLR_GPU<T>::XgetrsBatched(cublasOperation_t trans, int n, int nrhs, 
        const double *const Aarray[], int lda, const int *devIpiv, 
        double *const Barray[], int ldb, int *info, int batchSize) {
    CHECK_CUBLAS( cublasDgetrsBatched(handle, trans, n, nrhs, Aarray, lda,
                devIpiv, Barray, ldb, info, batchSize) );
}


template <typename T>
void HODLR_GPU<T>::XgetrsBatched(cublasOperation_t trans, int n, int nrhs, 
        const float *const Aarray[], int lda, const int *devIpiv, 
        float *const Barray[], int ldb, int *info, int batchSize) {
    CHECK_CUBLAS( cublasSgetrsBatched(handle, trans, n, nrhs, Aarray, lda,
                devIpiv, Barray, ldb, info, batchSize) );
}


template <typename T>
void HODLR_GPU<T>::XgetrsBatched(cublasOperation_t trans, int n, int nrhs, 
        const Complex *const Aarray[], int lda, const int *devIpiv, 
        Complex *const Barray[], int ldb, int *info, int batchSize) {
    CHECK_CUBLAS( cublasCgetrsBatched(handle, trans, n, nrhs, Aarray, lda,
                devIpiv, Barray, ldb, info, batchSize) );
}


template <typename T>
void HODLR_GPU<T>::XgetrsBatched(cublasOperation_t trans, int n, int nrhs, 
        const Zomplex *const Aarray[], int lda, const int *devIpiv, 
        Zomplex *const Barray[], int ldb, int *info, int batchSize) {
    CHECK_CUBLAS( cublasZgetrsBatched(handle, trans, n, nrhs, Aarray, lda,
                devIpiv, Barray, ldb, info, batchSize) );
}


template <typename T>
void HODLR_GPU<T>::factorize() {
    
    //tprint(N, C, U, "U");
    //tprint(n, N, D, "D");

    double gflop, f_total = 0;
    TimerGPU t, t0, t1, t2;

    t0.start();
    t.start();
    // leaf level
    int lda = n;
    ivec infoArray(m);
    auto ptrInfo = thrust::raw_pointer_cast(infoArray.data());
    XgetrfBatched(n, ptrD, lda, ptrPivotsD, ptrInfo, m);
    t.stop();
    double t_leaf_lu = t.elapsed_time();
    
    t.start();
    TVEC<T*> Barray(m);
    auto zero = thrust::make_counting_iterator<int>(0);
    thrust::transform(zero, zero+m, Barray.begin(), Stride<T>(ptrU, n));
    T **ptrB = thrust::raw_pointer_cast(Barray.data());

    int nrhs = C, ldb = N, info;
    XgetrsBatched(TRANSN, n, nrhs, ptrD, lda, ptrPivotsD, ptrB, ldb, &info, m);
    assert(info == 0);
    t.stop();
    double t_leaf_solve = t.elapsed_time();
    
    double f_leaf_lu = 1e-9*CFLOP*N*n*n/3;
    double f_leaf_solve = 1e-9*CFLOP*N*n*colptr[L];
    f_total += f_leaf_lu+f_leaf_solve;

    std::cout<<"\n------------------------------------------------"
        <<"\nleaf factor: "<<t_leaf_lu<<" s, Gflop/s: "<<f_leaf_lu/t_leaf_lu
        <<"\nleaf solve: "<<t_leaf_solve<<" s, Gflop/s: "<<f_leaf_solve/t_leaf_solve
        <<std::endl;
    
    //tprint(N, C, U, "U");
    //tprint(n, N, D, "D");

    //t.start();
    // compute diagonal blocks in K
    TVEC<T> VtU;
        
    // right-hand sides to be solved
    TVEC<T> Vtd;
    //t.stop();
    //std::cout<<"alloc memory time: "<<t.elapsed_time()<<std::endl;

    t1.start();
    for (int j=L-1; j>=0; j--) {
        
        //tprint(N, C, U, "U");
        std::cout<<"level: "<<j<<std::endl;
        
        int r = ranks[j];
        int ng = 1<<(j+1); // number of gemms
        VtU.resize(r*r*ng);
        T *ptrVtU = thrust::raw_pointer_cast(VtU.data());

        t2.start();
        t.start();
        T *A = ptrV+N*colptr[j];
        T *B = ptrU+N*colptr[j];
        XgemmBatched(TRANST, TRANSN, r, r, N/ng, 
                &ONE, A, N, N/ng, B, N, N/ng, &ZERO, ptrVtU, r, r*r, ng, fBatch);
        t.stop();

        gflop = 1e-9*CFLOP*r*r*N;
        f_total += gflop;
        std::cout<<"VtU time: "<<t.elapsed_time()<<" s, Gflop/s: "
            <<gflop/t.elapsed_time()<<std::endl;
        
        //tprint(r, r*m, VtU, "VtU");
        t.start();
        // copy vtu to subblocks in K 
        {
            auto K_begin = K.begin() + lvp[j];
            auto iter = thrust::make_transform_iterator(zero, KDB(r));
            auto kitr = thrust::make_permutation_iterator(K_begin, iter);
            thrust::copy_n(VtU.begin(), r*r*ng, kitr);
            
            // identities
            auto iter2 = thrust::make_transform_iterator(zero, KOB(r));
            auto kitr2 = thrust::make_permutation_iterator(K_begin, iter2);
            thrust::fill_n(kitr2, 2*r*ng/2, ONE);
        }
        t.stop();
        std::cout<<"form K time: "<<t.elapsed_time()<<" s, GB/s: "
            <<1e-9*r*r*ng*sizeof(T)/t.elapsed_time()<<std::endl;
        
        //tprint(2*r, 2*r*nk, K, "nonleaf K");

        // factorization
        int numK = (1<<j) - 1;
        T **ptrK = thrust::raw_pointer_cast(Karray.data() + numK);
        int *ptrPivots = ptrPivotsK[j];
        
        t.start();
        XgetrfBatched(2*r, ptrK, 2*r, ptrPivots, ptrInfo, ng/2);
        t.stop();

        gflop = 1e-9*CFLOP/3*4*r*r*r*ng;
        f_total += gflop;
        std::cout<<"factor K time: "<<t.elapsed_time()<<" s, Gflop/s: "
            <<gflop/t.elapsed_time()<<std::endl;

        // solve systems at non-leaf nodes
        if (j==0) break;

        t.start();
        // gemm
        Vtd.resize(colptr[j]*r*ng);
        T *ptrVtd = thrust::raw_pointer_cast(Vtd.data());

        nrhs = colptr[j];
        int ldc = r*ng;
        XgemmBatched(TRANST, TRANSN,
                    r, nrhs, N/ng, &ONE, A, N, N/ng, ptrU, N, N/ng, &ZERO, ptrVtd,
                    ldc, r, ng, fBatch);
        t.stop();

        gflop = 1e-9*CFLOP*r*nrhs*N;
        f_total += gflop;
        std::cout<<"Vtd time: "<<t.elapsed_time()<<" s, Gflop/s: "
            <<gflop/t.elapsed_time()<<std::endl;

        // solve
        TVEC<T*> VtdArray(ng/2);
        thrust::transform(zero, zero+ng/2, VtdArray.begin(), Stride<T>(ptrVtd, 2*r));
        T **ptrVtdArray = thrust::raw_pointer_cast(VtdArray.data());
        
        t.start();
        XgetrsBatched(CUBLAS_OP_N, 2*r, nrhs, ptrK,
                    2*r, ptrPivots, ptrVtdArray, ldc, &info, ng/2);
        assert(info == 0);
        t.stop();

        gflop = 1e-9*CFLOP*2*r*r*nrhs*ng;
        f_total += gflop;
        std::cout<<"K solve time: "<<t.elapsed_time()<<" s, Gflop/s: "
            <<gflop/t.elapsed_time()<<std::endl;

        t.start();
        // gemm
        XgemmBatched(TRANSN, TRANSN,
                    N/ng, nrhs, r, &NEGONE, B, N, N/ng, ptrVtd, ldc, r, &ONE, ptrU,
                    N, N/ng, ng, -1/*no stream*/);
        t.stop();
        t2.stop();
        
        gflop = 1e-9*CFLOP*N*nrhs*r;
        f_total += gflop;
        std::cout<<"update U time: "<<t.elapsed_time()<<" s, Gflop/s: "
            <<gflop/t.elapsed_time()<<std::endl;
        std::cout<<"level time: "<<t2.elapsed_time()<<std::endl;
    }
    t1.stop();
    std::cout<<"loop time: "<<t1.elapsed_time()<<std::endl;
    t0.stop();
    double t_total = t0.elapsed_time();
    double t_else = t_total-t_leaf_lu-t_leaf_solve;

    //tprint(N, C, U, "U");
    //tprint(N, C, V, "V");
    //tprint(2*r, 2*r*nk, K, "K");

    std::cout<<"\n------------------------------------------------"
        <<"\ntotal: "<<t_total<<" s, Gflop/s: "<<f_total/t_total
        //<<"\nelse: "<<t_else<<" s, "<<t_else/t_total*100<<" %"
        <<"\n------------------------------------------------\n\n";
}


template <typename T>
void HODLR_GPU<T>::solve(T *hb, int nrhs) {

    double Gflop, Tflop = 0;
    TimerGPU t, t1, t2;

    TVEC<T> b(N*nrhs);
    T *ptrB = thrust::raw_pointer_cast(b.data());
    
    t.start();
    thrust::copy_n(hb, N*nrhs, b.begin());
    t.stop();
    std::cout<<"\n------------------------------------------------\n";
    std::cout<<"Copy RHS to GPU: "<<t.elapsed_time()<<" s, GB/s: "
        <<1e-9*N*nrhs*sizeof(T)/t.elapsed_time()<<std::endl;

    t2.start();
    TVEC<T*> Barray(m);
    auto Zero = thrust::make_counting_iterator<int>(0);
    thrust::transform(Zero, Zero+m, Barray.begin(), Stride<T>(ptrB, n));
    T **ptrBarray = thrust::raw_pointer_cast(Barray.data());

    t.start();
    int lda = n, ldb = N, info;
    XgetrsBatched(TRANSN, n, nrhs, ptrD, lda, ptrPivotsD, ptrBarray, ldb, &info, m);
    assert(info == 0);
    t.stop();
    Gflop = 1e-9*CFLOP*N*n; Tflop += Gflop;
    std::cout<<"\nleaf solve: "<<t.elapsed_time()<<" s, Gflop/s: "
        <<Gflop/t.elapsed_time()<<std::endl;

    // right-hand sides to be solved
    TVEC<T> Vtd;
    for (int j=L-1; j>=0; j--) {

        std::cout<<"level: "<<j<<std::endl;

        t1.start();
        int r = ranks[j];
        int ng = 1<<(j+1); // number of gemms

        Vtd.resize(r*nrhs*m);
        T *ptrVtd = thrust::raw_pointer_cast(Vtd.data());

        // gemm
        t.start();
        int ldc = r*ng;
        T   *A  = ptrV+N*colptr[j];
        XgemmBatched(TRANST, TRANSN, r, nrhs, N/ng, &ONE,
                A, N, N/ng, ptrB, N, N/ng, &ZERO, ptrVtd, ldc, r, ng, sBatch);

        t.stop();
        Gflop = 1e-9*CFLOP*r*N*nrhs; Tflop += Gflop;
        std::cout<<"Vtd time: "<<t.elapsed_time()<<" s, Gflop/s: "
            <<Gflop/t.elapsed_time()<<std::endl;

        // solve
        t.start();
        int numK = (1<<j) - 1;
        T  **ptrK = thrust::raw_pointer_cast(Karray.data() + numK);
        int *ptrPivots = ptrPivotsK[j];

        TVEC<T*> VtdArray(ng/2);
        thrust::transform(Zero, Zero+ng/2, VtdArray.begin(), Stride<T>(ptrVtd, 2*r));
        T **ptrVtdArray = thrust::raw_pointer_cast(VtdArray.data());
        XgetrsBatched(CUBLAS_OP_N, 2*r, nrhs, ptrK,
                    2*r, ptrPivots, ptrVtdArray, ldc, &info, ng/2);
        assert(info == 0);
        t.stop();
        Gflop = 1e-9*CFLOP*2*r*r*nrhs*ng; Tflop += Gflop;
        std::cout<<"K solve time: "<<t.elapsed_time()<<" s, Gflop/s: "
            <<Gflop/t.elapsed_time()<<std::endl;

        // gemm
        t.start();
        T *B = ptrU+N*colptr[j];
        XgemmBatched(TRANSN, TRANSN,
                    N/ng, nrhs, r, &NEGONE, B, N, N/ng, ptrVtd, ldc, r, &ONE, ptrB,
                    N, N/ng, ng, -1/*no stream*/);
        t.stop();
        Gflop = 1e-9*CFLOP*r*N*nrhs; Tflop += Gflop;
        std::cout<<"Update U time: "<<t.elapsed_time()<<" s, Gflop/s: "
            <<Gflop/t.elapsed_time()<<std::endl;

        t1.stop();
        std::cout<<"level time: "<<t1.elapsed_time()<<std::endl;
    }
    t2.stop();

    t.start();
    thrust::copy_n(b.begin(), N*nrhs, hb);
    t.stop();
    std::cout<<"\nCopy RHS to CPU: "<<t.elapsed_time()<<" s, GB/s: "
        <<1e-9*N*nrhs*sizeof(T)/t.elapsed_time()<<std::endl;


    std::cout<<"\n------------------------------------------------"
        <<"\ntotal: "<<t2.elapsed_time()<<" s, Gflop/s: "<<Tflop/t2.elapsed_time()
        //<<"\nelse: "<<t_else<<" s, "<<t_else/t_total*100<<" %"
        <<"\n------------------------------------------------\n\n";
}


template <typename T>
HODLR_GPU<T>::~HODLR_GPU() {
    CHECK_CUBLAS( cublasDestroy(handle) );
}

#endif
