#include <algorithm>
#include <iostream>
#include <random>
#include <vector>

#include "hodlr_gpu.hpp"

typedef cuComplex Complex;
typedef cuDoubleComplex Zomplex;


int main(int argc, char *argv[]) {

#if 0
    int n = 64; // matrix size
    int r = 20;
    int L = 12;
#else
    int n = 32; // matrix size
    int r = 2;
    int L = 8;
#endif

    int nrhs = 1;
    
    int m = 1<<L; // number of matrices
    int N = n*m, C = r*L;
    std::vector<int> ranks(L, r);
        
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_real_distribution<double> dist(0,1);
    
    std::vector<double> dU(N*C), dV(N*C), dD(N*n), dB(N*nrhs);
    for (int i=0; i<N*C; i++) {
        dU[i] = dist(rng);
        dV[i] = dist(rng);
    }
    for (int i=0; i<N*n; i++) {
        dD[i] = dist(rng);
        if (i%(n*n)/n == i%(n*n)%n)
            dD[i] += 100;
    }
    for (int i=0; i<N*nrhs; i++)
        dB[i] = dist(rng);

    HODLR_GPU<double> dHmat(N, n, L, ranks);
    dHmat.initialize(dU.data(), dV.data(), dD.data());
    dHmat.factorize();

    std::vector<double> dX(N*nrhs);
    std::copy(dB.begin(), dB.end(), dX.begin());
    dHmat.solve(dX.data(), nrhs);

    // !!! single precision

    std::vector<float> fU(N*C), fV(N*C), fD(N*n), fX(N*nrhs);
    for (int i=0; i<N*C; i++) {
        fU[i] = dU[i];
        fV[i] = dV[i];
    }
    for (int i=0; i<N*n; i++)
        fD[i] = dD[i];
    for (int i=0; i<N*nrhs; i++)
        fX[i] = dB[i];

    HODLR_GPU<float> fHmat(N, n, L, ranks);
    fHmat.initialize(fU.data(), fV.data(), fD.data());
    fHmat.factorize();
    fHmat.solve(fX.data(), nrhs);

    double nrm = 0, err = 0;
    for (int i=0; i<N*nrhs; i++) {
        nrm += dX[i]*dX[i];
        err += (dX[i]-fX[i])*(dX[i]-fX[i]);
    }
    std::cout<<"Single precision error: "<<sqrt(err/nrm)<<std::endl;
    
    std::cout<<"dX: ";
    for (int i=0; i<3; i++)
        std::cout<<dX[i]<<" ";
    std::cout<<std::endl;

    std::cout<<"fX: ";
    for (int i=0; i<3; i++)
        std::cout<<fX[i]<<" ";
    std::cout<<std::endl;

    /*
    // !!! half precision
    
    std::vector<__half> hU(N*C), hV(N*C), hD(N*n), hX(N*nrhs);
    for (int i=0; i<N*C; i++) {
        hU[i] = dU[i];
        hV[i] = dV[i];
    }
    for (int i=0; i<N*n; i++)
        hD[i] = dD[i];
    for (int i=0; i<N*nrhs; i++)
        hX[i] = dB[i];

    HODLR_GPU<__half> hHmat(N, n, L, ranks);
    hHmat.initialize(hU.data(), hV.data(), hD.data());
    hHmat.factorize();
    hHmat.solve(hX.data(), nrhs);

    nrm = 0, err = 0;
    for (int i=0; i<N*nrhs; i++) {
        nrm += dX[i]*dX[i];
        err += (dX[i]-double(hX[i]))*(dX[i]-double(hX[i]));
    }
    std::cout<<"Half precision error: "<<sqrt(err/nrm)<<std::endl;

    std::cout<<"hX: ";
    for (int i=0; i<3; i++)
        std::cout<<double(hX[i])<<" ";
    std::cout<<std::endl;
    */

    //std::vector<Zomplex> zU(N*C), zV(N*C), zD(N*n), zB(N*nrhs);
    //HODLR_GPU<Zomplex> zHmat(N, n, L, ranks);
    //zHmat.initialize(zU.data(), zV.data(), zD.data());
    //zHmat.factorize();

    

    return 0;
}


