
EXE = kernel bie

VFLAGS = -DUSE_DOUBLE -arch=compute_70 -code=sm_70 -Xcompiler -std=c++14 --default-stream per-thread

LFLAGS = -L$(CUDADIR)/lib64 -lcublas -lcuda -lcudart

INCLUDES = -I./gpu

all: $(EXE)

%.o: %.cu %.hpp
	nvcc -c $< -o $@ $(VFLAGS) $(INCLUDES)

%.o: %.cu
	nvcc -c $< -o $@ $(VFLAGS) $(INCLUDES)

kernel.o: kernel.cpp
	cd cpu; make -j9
	g++ -c $< -o $@ \
		-I./cpu -I$(EIGEN_ROOT) -DUSE_DOUBLE --std=c++14 -Wall -O3 -fopenmp \
		-DEIGEN_USE_MKL_ALL -DMKL_ILP64 -m64 -I"${MKLROOT}/include"

kernel: kernel.o gpu_solver.o gpu/timer_gpu.o 
	g++ $^ -o $@ $(LFLAGS) \
		-L./cpu/ -Wl,-rpath=./cpu/ -lhodlr \
		-liomp5 -lpthread -lm -ldl \
		-L${MKLROOT}/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm -ldl

bie: bie.o gpu/timer_gpu.o
	g++ $^ -o $@ $(LFLAGS)

clean:
	rm -f $(EXE) *.o gpu/timer_gpu.o


